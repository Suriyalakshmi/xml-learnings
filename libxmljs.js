var libxml = require("libxmljs");
var xml =  '<?xml version="1.0" encoding="UTF-8"?>' +
"<Company>"+
"<Employee>"+
      "<FirstName>Tanmay</FirstName>"+
      "<LastName>Patil</LastName>"+
      "<ContactNo>1234567890</ContactNo>"+
      "<Email>tanmaypatil@xyz.com</Email>"+
      "<Address>"+
         "<City>Bangalore</City>"+
         "<State>Karnataka</State>"+
         "<Zip>560212</Zip>"+
      "</Address>"+
   "</Employee>"+
"</Company>"

var xmlDoc = libxml.parseXmlString(xml);
var LS = xmlDoc.get('//LastName');

console.log(LS.text());

/*OUTPUT;
PS C:\Users\Suriyalakshmi> node libxmljs.js
Patil*/

