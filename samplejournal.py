from bs4 import BeautifulSoup
import json
InputXML = open("samplexml.xml","r")
contents = InputXML.read()
soup = BeautifulSoup(contents,'xml')

family = soup.find_all('surname')
given=soup.find_all('given-names')

for i in range(0, len(family)):
    author={"family" :family[i].get_text(),
        "given-name" : given[i].get_text()
       }
    print(json.dumps(author))

art=soup.find_all('article-title')
for article in art:
  art_tit={
      "title" : article.get_text()  }
  print(json.dumps(art_tit))

volume=soup.find_all('volume')
for vol in volume:
  volumes={
      "volume" : vol.get_text()  }
  print(json.dumps(volumes))

date=soup.find_all('year')
for dates in date:
  pub_date={
      "date" : dates.get_text()  }
  print(json.dumps(pub_date))
  
