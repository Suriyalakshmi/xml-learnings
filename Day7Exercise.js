const http = require('http');
const fs = require('fs');
const xpath = require("xpath-html");
const html = fs.readFileSync(`${__dirname}/sample.html`, 'utf8');

const node1 = xpath.fromPageSource(html).findElements("//h1[@class='jrnlArtTitle']/span[@title[starts-with(.,upper-case)]]");
console.log(node1.toString());
if (node1.toString() !== null) {
    console.log("Title starts with uppercase");
} else {
    console.log("Title  doesn't starts with uppercase");
}

const node2 = xpath.fromPageSource(html).findElements("//h1[@class='jrnlArtTitle']/span[@title[not(ends-with(.,'.'))]]");
console.log(node2.toString());
if (node2.toString() !== null) {
    console.log("Dot is not used in the title");
} else {
    console.log("Dot is used in the title");
}

const node3a = xpath.fromPageSource(html).findElements("//span[@class='jrnlAuthorGroup']/span[@class='jrnlAuthor']/span[@class='jrnlSurName']//text()[(..)[true()]]");
console.log(node3a.toString());
if (node3a.toString() !== null) {
    console.log("Authors are inside the jrnlAuthorGroup");
} else {
    console.log("Authors aren't inside the jrnlAuthorGroup");
}

const node3b = xpath.fromPageSource(html).findElements("//sup[@class='jrnlAffRef']//text()[(..)[true()]]");
console.log(node3b.toString(), "are the arabic numbers");
if (node3b.toString() !== null) {
    console.log("Authors are linked with superscript arabic numerals");
} else {
    console.log("Authors aren't linked with superscript arabic numerals");
}

const node4 = xpath.fromPageSource(html).findElements("//span[@class='jrnlAuthorGroup']/span[@class='jrnlDegree']"); 
console.log(node4.toString());
if (node4.toString()!== null) {
    console.log("There is no such tag found");
} else {
    console.log("found");
}

const node5 = xpath.fromPageSource(html).findElements("//span[@class='jrnlPrefix'][contains(text(),Mr.) or contains(text(),Dr.)]//text()[(..)[true()]]"); 
console.log(node5.toString());
if (node5.toString() !== null) {
    console.log("present");
} else {
    console.log("not present");
}

const node6 = xpath.fromPageSource(html).findElements("//span[@corresp='yes']/span[position()<3]");
console.log(node6.toString());
if (node6.toString() !== null) {
    console.log("Up to two corresponding authors ");
} else {
    console.log("Doesn't follow the condition");
}


const node7 = xpath.fromPageSource(html).findElements("//h1[@class='jrnlAbsHead'][contains(text(),'Abstract')]");
//console.log(node7.toString());
if (node7.toString() !== null) {
    console.log("Headings are in abstract");
} else {
    console.log("Headings are not in abstract");
}

const node8 = xpath.fromPageSource(html).findElements("//h1[@class='jrnlAbsHead']/span[@class='jrnlBibRef']");
console.log(node8.toString());
if (node8.toString() !== null) {
    console.log("Reference citations aren't in abstract");
} else {
    console.log("Reference citations are in abstract");
}

const node9 = xpath.fromPageSource(html).findElement("//h1[@class='jrnlHead1'][not(starts-with(.,'1'and'2'))]");
var d = node9.getText();
if (d != null) {
    console.log("Numbers are not used in the beginning");
} else {
    console.log("Numbers are used in the beginning");
}

const node10 = xpath.fromPageSource(html).findElements("//p[@class='jrnlFigCaption']/span[@title][ends-with(.,'.')]");
var d = node10.toString();
if (d != null) {
    console.log("Caption ends with dot");
} else {
    console.log("Captions does not ends with dot");
}
console.log(node10.toString());

