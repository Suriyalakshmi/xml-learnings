from bs4 import BeautifulSoup
import json
Inputfile = open("samplexml.xml","r")
contents = Inputfile.read()
soup = BeautifulSoup(contents,'xml')

def surname(a):
  name={}
  if a.find('surname'):
    name['family']=a.find('surname').get_text()
  if a.find('given-names'):
    name['given']=a.find('given-names').get_text() 
  return name

def names(reference):
  author=[]
  if reference.find('person-group'):
    names=reference.find('person-group').find_all('name')
    for name in names:
      authorname=surname(name)
      author.append(authorname)
    collabs=reference.find('person-group').find_all('collab')
    for collab in collabs:
      authorname={}
      authorname['name']=collab.get_text()
      author.append(authorname)
  return author

def titles(reference):
  title=[]
  if reference.find('article-title'):
    t=reference.find('article-title').text.replace(' ','').replace('\n',' ')
    title.append(t)
  return title

def volume(reference):
  vol=[]
  if reference.find('volume'):
    v=reference.find('volume').get_text()
    vol.append(v)
  return vol
 
def years(reference):
  year=[]
  if reference.find('year'):
    y=reference.find('year').get_text()
    year.append(y)
    return year
  
ref=soup.find_all('element-citation')
references=[]
for ref_tag in ref:
  reference={}
  reference['author']=names(ref_tag)
  reference['title']=titles(ref_tag)
  reference['Volume']=volume(ref_tag)
  reference['type']="journal"
  reference['date']=years(ref_tag)
  references.append(reference)

final={"references" :references}
ans=json.dumps(final,indent=4)

with open('Outputfile.json','w') as outfile:
  outfile.write(ans)
