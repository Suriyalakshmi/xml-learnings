const node_xml_stream = require('node-xml-stream');
const parser = new node_xml_stream();
const fs = require('fs');
let user = { 'USER': [] };
let driver, team, attr, year, t_name;
parser.on('opentag', function(name, attrs) {
    if(name === 'USER') {
        attr = attrs;
    }
    t_name = name;
});
parser.on('closetag', function(name) {
    if(name === 'USER') {
        user['USER'].push({ "name": driver, "team": team, "year": attr.year });
    }
});
parser.on('text', function(text) {
    if(t_name === 'NAME') {
        driver = text;
    }
    if(t_name === 'TEAM') {
        team = text;
    }
});
parser.on('finish', function() {
    console.log(user);
});
let stream = fs.createReadStream('data.xml', 'UTF-8');
stream.pipe(parser);

/*PS C:\Users\Suriyalakshmi> node Read-xml-using-stream.js
{
  USER: [
    { name: 'VETTEL', team: 'FERRARI', year: '2018' },
    { name: 'RAIKKONEN', team: 'FERRARI', year: '2018' },
    { name: 'HAMILTON', team: 'MERCEDES', year: '2018' },
    { name: 'BOTTAS', team: 'MERCEDES', year: '2018' },
    { name: 'RICCIARDO', team: 'REDBULL', year: '2018' },
    { name: 'VERSTAPPEN', team: 'REDBULL', year: '2018' }
  ]
}*/