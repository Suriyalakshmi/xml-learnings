const fs = require('fs');
var xpath = require('xpath'),
    dom = require('xmldom').DOMParser
const xml = fs.readFileSync(`${__dirname}/samp.xml`, "utf8");
var doc = new dom().parseFromString(xml);
var ans = xpath.select("//fig/label[contains(.,'Figure 1')]", doc);
console.log(ans);
fs.writeFile('output.xml', ans.toString(), function(err) {
    if (err) throw err;
    console.log('File created');
});