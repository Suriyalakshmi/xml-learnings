const xml2js = require('xml2js');
const fs = require('fs');
const util = require('util');
const parser = new xml2js.Parser({ attrkey: "ATTR" });
fs.readFile('data.xml', (err,data) => {
    if(!err) {
        parser.parseString(data, function(error, result) {
            if(error === null) {
                console.log(util.inspect(result,false,null));
            }
            else {
                console.log(error);
            }
        });
    }
});

/*{
  FORMULA1: {
    USER: [
      {
        ATTR: { year: '2018' },
        NAME: [ 'VETTEL' ],
        TEAM: [ 'FERRARI' ]
      },
      {
        ATTR: { year: '2018' },
        NAME: [ 'RAIKKONEN' ],
        TEAM: [ 'FERRARI' ]
      },
      {
        ATTR: { year: '2018' },
        NAME: [ 'HAMILTON' ],
        TEAM: [ 'MERCEDES' ]
      },
      {
        ATTR: { year: '2018' },
        NAME: [ 'BOTTAS' ],
        TEAM: [ 'MERCEDES' ]
      },
      {
        ATTR: { year: '2018' },
        NAME: [ 'RICCIARDO' ],
        TEAM: [ 'REDBULL' ]
      },
      {
        ATTR: { year: '2018' },
        NAME: [ 'VERSTAPPEN' ],
        TEAM: [ 'REDBULL' ]
      }
    ]
  }
}*/