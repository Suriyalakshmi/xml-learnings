const http = require('http');
const fs = require('fs');
const xpath = require("xpath-html");
const html = fs.readFileSync(`${__dirname}/sample.html`, 'utf8');

const node6 = xpath.fromPageSource(html).findElements("//span[@corresp='yes']/span[position()<3]");
console.log(node6.toString());
if (node6.toString() !== null) {
    console.log("Up to two corresponding authors ");
} else {
    console.log("Doesn't follow the condition");
}
